<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-09-05
 * Time: 18:30
 */
 

$app->get('/article', function () use ($app) {

    $articles = $app->article->all();

    $app->render('article/articles.twig',
	[
	    'article' => $articles
	]);

})->name('article');


$app->get('/article/:id', function ($id) use ($app) {

    $article = $app->article->where('id', $id)->first();

    $app->render('article/article.twig',
	[
	    'article' => $article
	]);

})->name('article.id');

/**
 * Add a new article
 */
$app->post('/article/add', function () use ($app) {

    $request = $app->request;

    $app->article->create([
	'title' => $request->post('title'),
	'body' => $request->post('body'),
	'author' => $request->post('author')
    ]);

    $app->response->redirect($app->urlFor('article'));

})->name('article.add');


/**
 * Update an article
 */
$app->post('/article/update/:id', function ($id) use ($app) {

    $request = $app->request;

    $article = $app->article->where('id', $id)->first();

    $article->title = $request->post('title');
    $article->title = $request->post('body');
    $article->title = $request->post('author');

    $article->save();

    $app->response->redirect($app->urlFor('article'));

})->name('article.update.id');

/**
 * Delete an article
 */
$app->delete('/article/delete/:id', function ($id) use ($app) {

    $article = $app->article->where('id', $id)->first();

    $article->delete();

    $app->response->redirect($app->urlFor('article'));

})->name('article.delete.id');