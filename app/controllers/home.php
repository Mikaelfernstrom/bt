<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-09-05
 * Time: 18:30
 */

$app->get('/', function() use ($app) {
    $app->log->info("Home '/' route" );
  $app->render('home.twig');
})->name('home');
