<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-26
 * Time: 22:07
 */
namespace Core\Article;
use Illuminate\Database\Eloquent\Model as Model;

class Article extends Model {

    protected $table = 'article';
    protected  $fillable = [
	'author', 'title', 'body'
    ];

}