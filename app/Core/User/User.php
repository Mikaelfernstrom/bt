<?php
/**
 * Created by PhpStorm.
 * User: lars
 * Date: 2015-08-24
 * Time: 22:21
 */

namespace Core\User;
use Illuminate\Database\Eloquent\Model as Model;

class User extends Model {

    protected $table = 'users';
    protected  $fillable = [
	'email', 'username', 'password', 'active', 'active_hash', 'remember_identifier', 'remember_token'
    ];

    public function _construct(){

    }

}